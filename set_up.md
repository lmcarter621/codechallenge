Abl: Code Challenge by Lauren Carter
======================================

## Setup and Run Directions
**This webapp should be run from within in a directory called codechallenge (i.e. the directory where the repo was cloned should be called codechallenge)**  
1. From the terminal, install virtualenv by running the following:  
	`sudo pip install virtualenv`  
1. From within the codechallenge directory, create the virtual environment and install the requirements by running the following:  
	```bash
		virtualenv ccenv
		. ccenv/bin/activate
		sudo pip install -r requirements.txt
	```  
1. Tell Flask where the application is located.  
	```bash
		export FLASK_APP=index.py
	```  
1. Start the web app with  
	```bash
		flask run
	```  
1. Navigate to [http://127.0.0.1:5000/](http://127.0.0.1:5000/) and click 'Run.'  

## What I would do differently
* This is a pretty ad-hoc webapp, but it did give me the opportunity to work with Flask.  It's also pretty small for building something more complex, so I would definitely lay out the routing better.  I would probably make index.py a controller and set up proper configurations for a webapp given more time (and more complex problem).  index.py would become some type of controller type file that handles incoming and outgoing HTTPRequests.  I also would have come up with a better UI.  It's pretty raw because it just gets the point across but given a more complex problem, the UI could use a bit more work.  I'd definitely add a way to pass in different URLs.
* I'm not 100% satisfied with the ApiManager.  This is something I would have brainstormed with another engineer on.  It fits for now but I'm convinced collaboration would help me come to a better solution for this.
* I would create permantent test fixtures for the tests.  Some of this code is repeated and could by DRY-ed up, particularly for things like models.