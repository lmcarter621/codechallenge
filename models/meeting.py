class Meeting(object):
	"""Representation of Meeting"""
	def __init__(self, id, name, duration, period, days_of_week=[], tags=[]):
		super(Meeting, self).__init__()
		self.name = name
		self.duration = duration
		self.period = period
		self.days_of_week=days_of_week
		self.tags=tags
		