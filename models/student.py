class Student(object):
	"""Representation of a Student"""
	def __init__(self, name):
		super(Student, self).__init__()
		self.name = name
		self.meetings = []

	def get_meetings_by_tag(self):
		"""
			Returns a dictionary of meetings categorized by tag
		"""
		meetings_by_tag = {}
		for m in self.meetings:
			for tag in m.tags:
				tag_list = meetings_by_tag.get(tag)
				if tag_list is None:
					meetings_by_tag[tag] = [m]
				else:
					tag_list.append(m)

		return meetings_by_tag
		