Code Challenge
=================================

The goal of this code challenge to complete a code challenge - build a program according to a set of requirements.  The actual business requirements are linked below, but here are the general tech requirements:

1. Write your code using whatever language or framework you feel most comfortable in.
1. It doesn't matter if you build this as a front-end app, a service, a command line program, etc.
1. Make sure your solution is runnable on Mac OS X or Linux.
1. Do not require any for-pay software.
1. Include instructions for setting up and running your program.
1. Make sure you write some tests (100% coverage is not required).


## The Problem

Write a simple program that does the following:

1) Takes in a student name as input
2) Gets that student's schedule from an API (endpoint URLs that were provided)
3) Outputs the percentage of time spent in meetings tagged with "instructional"


Things that were paramount:

* Documenting the method for setting up and running your application
* Following the instructions for submission
* Fulfilling the basic requirements
* Breaking the problem into smaller parts and clearly connecting each part (separating your concerns)
* What approach you take to testing
* How you organize your code
* What you would do differently if this was for real (no time constraints, etc.)


Less important factors:

* Arriving at a specific answer or working through all of the edge cases
* Lots of features or anything impressive.  It is ok to keep it simple.
* Creating something beautifully styled and aesthetically pleasing
