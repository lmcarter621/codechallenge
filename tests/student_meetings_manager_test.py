import unittest

from codechallenge.business_managers.student_meetings_manager import StudentMeetingsManager
from codechallenge.models.student import Student
from codechallenge.models.meeting import Meeting

class StudentsMeetingsManagerTests(unittest.TestCase):
	def setUp(self):
		self.student_meeting_mgr = StudentMeetingsManager()
		self.student = Student(name='Bob')

		meeting1 = Meeting(id=1, name='Arrival', duration=30, period=0, days_of_week=['regular_day', 'short_day'])
		meeting2 = Meeting(id=2, name='Science', duration=45, period=1,
			days_of_week=['regular_day', 'short_day'], tags=['core', 'instructional'])
		meeting3 = Meeting(id=3, name='English', duration=45, period=2,
			days_of_week=['regular_day', 'short_day'], tags=['core', 'instructional'])
		meeting4 = Meeting(id=4, name='PE', duration=60, period=3,
			days_of_week=['regular_day'], tags=['pe'])
		self.meetings = [meeting1, meeting2, meeting3, meeting4]
		
	def test_add_meetings_to_student(self):
		"""
			When meetings are given as a list of objects, verify they can be added 
			to the student as objects
		"""
		meetings = [{
				  'days_of_week': ['regular_day', 'short_day'],
				  'duration': 30,
				  'id': 1,
				  'name': 'Arrival',
				  'period': 0,
				  'tags': []
			  },
			 {
			 	  'days_of_week': ['regular_day', 'short_day'],
			  	  'duration': 45,
			  	  'id': 2,
			  	  'name': 'Science',
			  	  'period': 1,
			  	  'tags': ['core', 'instructional']
			 },
 			{
 				 'days_of_week': ['regular_day', 'short_day'],
			  	 'duration': 45,
			     'id': 3,
			     'name': 'English',
			     'period': 2,
			     'tags': ['core', 'instructional']
			},
			{
				 'days_of_week': ['regular_day'],
  				 'duration': 60,
  				 'id': 4,
  				 'name': 'PE',
  				 'period': 3,
  				 'tags': ['pe']
  			}]

		self.student_meeting_mgr.add_meetings_to_student(self.student, meetings)
		self.assertEqual(len(self.student.meetings), len(meetings))

	def test_caluclate_time_in_meetings_by_tag(self):
		"""
			Given a student and a tag calculate the amount of time spent in that
			meeting type for the given student
		"""
		self.student.meetings = self.meetings
		tag_time_i = self.student_meeting_mgr.calculate_time_in_meetings_by_tag(self.student, 'instructional')
		self.assertEqual(tag_time_i, 90)

		tag_time_pe = self.student_meeting_mgr.calculate_time_in_meetings_by_tag(self.student, 'pe')
		self.assertEqual(tag_time_pe, 60)

	def test_percentage_time_in_meetings_by_tag(self):
		"""
			Given a student and a tag calculate the percentage of time in that
			meeting type for the given student
		"""
		self.student.meetings = self.meetings
		percentage_i = self.student_meeting_mgr.percentage_time_in_meetings_by_tag(self.student, 'instructional')
		self.assertEqual(percentage_i, 50)

		percentage_pe = self.student_meeting_mgr.percentage_time_in_meetings_by_tag(self.student, 'pe')
		self.assertEqual(percentage_pe, 33)
