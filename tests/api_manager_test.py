import unittest
import json
import urllib2
from mock import MagicMock, patch

from codechallenge.business_managers.api_manager import ApiManager
from codechallenge.business_managers import STUDENT_REQUEST_URL, MEETINGS_REQUEST_URL
from codechallenge.models.student import Student

class ApiManagerTests(unittest.TestCase):
	def setUp(self):
		self.mgr = ApiManager()

	@patch('codechallenge.business_managers.api_manager.urllib2')
	def test_fetch_students(self, mock_call):
		"""
			Fetches the list of names from the STUDENT_REQUEST_URL
		"""
		mock_call.urlopen = MagicMock()
		mock_call.urlopen.return_value.read.return_value = json.dumps(['Linda', 'Ashley', 'Lauren'])
		self.mgr.fetch_students()

		mock_call.urlopen.assert_called_with(STUDENT_REQUEST_URL)
		self.assertEqual(len(self.mgr.business_mgr.student_list), 3)

	@patch('codechallenge.business_managers.api_manager.urllib2')
	def test_fetch_meetings(self, mock_call):
		"""
			Fetches the list of meeting objects from the MEETINGS_REQUEST_URL
			for a given student
		"""
		mock_call.urlopen = MagicMock()
		meetings_list = [{
				  'days_of_week': ['regular_day', 'short_day'],
				  'duration': 30,
				  'id': 1,
				  'name': 'Arrival',
				  'period': 0,
				  'tags': []
			  },
			 {
			 	  'days_of_week': ['regular_day', 'short_day'],
			  	  'duration': 45,
			  	  'id': 2,
			  	  'name': 'Science',
			  	  'period': 1,
			  	  'tags': ['core', 'instructional']
			 },
 			{
 				 'days_of_week': ['regular_day', 'short_day'],
			  	 'duration': 45,
			     'id': 3,
			     'name': 'English',
			     'period': 2,
			     'tags': ['core', 'instructional']
			},
			{
				 'days_of_week': ['regular_day'],
  				 'duration': 60,
  				 'id': 4,
  				 'name': 'PE',
  				 'period': 3,
  				 'tags': ['pe']
  			}]
		mock_call.urlopen.return_value.read.return_value = json.dumps(meetings_list)

		self.mgr.business_mgr.student_list = [Student(name='Lauren')]
		self.mgr.fetch_meetings()

		mock_call.urlopen.assert_called_with(MEETINGS_REQUEST_URL.format('Lauren'))
		self.assertEqual(len(self.mgr.business_mgr.student_list[0].meetings), 4)
