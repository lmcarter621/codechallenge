from flask import Flask
from flask import request
from flask import render_template

from codechallenge.business_managers.api_manager import ApiManager
app = Flask(__name__)

@app.route('/results/')
def results():
	api_mgr = ApiManager()
	api_mgr.fetch_students()
	api_mgr.fetch_meetings()

	percents = {}
	for student in api_mgr.business_mgr.student_list:
		percents[student.name] = api_mgr.business_mgr.percentage_time_in_meetings_by_tag(student, 'instructional')

	return render_template('results.html', data=percents)

@app.route('/')
def success_route():
	return render_template('index.html')