import json
import urllib2

from codechallenge.business_managers.student_meetings_manager import StudentMeetingsManager
from codechallenge.models.student import Student
from codechallenge.business_managers import STUDENT_REQUEST_URL, MEETINGS_REQUEST_URL

class ApiManager(object):
	"""This class interacts with the various apis necessary to fetch and send data"""
	def __init__(self):
		super(ApiManager, self).__init__()
		self.business_mgr = StudentMeetingsManager()


	def fetch_students(self):
		"""
			Gets the list of students from the requested url.  Iterates over the list to create
			Student objects
		"""
		student_name_list = json.load(urllib2.urlopen(STUDENT_REQUEST_URL))

		self.business_mgr.student_list = [Student(name=s) for s in student_name_list]


	def fetch_meetings(self):
		"""
			Gets the list of meetings related to a particular student and assigns them to that
			student.
		"""
		for student in self.business_mgr.student_list:
			meeting_data_list = json.load(urllib2.urlopen(MEETINGS_REQUEST_URL.format(student.name)))
			self.business_mgr.add_meetings_to_student(student, meeting_data_list)
		