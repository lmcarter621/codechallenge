from codechallenge.models.meeting import Meeting

class StudentMeetingsManager(object):
	"""
	StudentMeetingsManager handes all of the business logic
	for students and meetings
	"""
	def __init__(self):
		super(StudentMeetingsManager, self).__init__()
		self.student_list = []

	def add_meetings_to_student(self, student, meetings):
		meetings = [Meeting(id=m['id'],
							name=m['name'],
							duration=m['duration'],
							period=m['period'],
							days_of_week=m['days_of_week'] if 'days_of_week' in m else [],
							tags=m['tags'] if 'tags' in m else []
					) for m in meetings]

		student.meetings = meetings

	def calculate_time_in_meetings_by_tag(self, student, tag):
		meetings_by_tag = student.get_meetings_by_tag()
		meetings_for_tag = meetings_by_tag.get(tag)

		tag_time = 0
		if meetings_for_tag is not None:
			for m in meetings_for_tag:
				tag_time += m.duration

		return tag_time

	def percentage_time_in_meetings_by_tag(self, student, tag):
		tag_time = self.calculate_time_in_meetings_by_tag(student, tag)

		total_duration = 0
		for m in student.meetings:
			total_duration += m.duration

		return round(float(tag_time)/total_duration * 100)
